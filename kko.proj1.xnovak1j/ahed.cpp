/*
 * Autor: Adrián Novák (xnovak1j)
 * Datum: 11.2.2018
 * Soubor: ahed.cpp
 * Komentar: implementacia adaptivneho Huffmanovho kodovania a dekodovania. Blizsi popis v ahed.h
 */

#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <queue>
#include <bitset>
#include "ahed.h"

void deleteTree(NODE *root) {
    if (root != nullptr){
        deleteTree(root->left);
        deleteTree(root->right);
        delete(root);
    }
}

void writeCoded(tAHED *ahed, std::string code, FILE *outputFile, int flag) {
    static int counter = 7;
    int myBit = 0;
    static std::bitset<8> codeBits; //zakodovane znaky vzdy po 8b zapis do suboru

    /*iteracia cez dlzku kodu kodovaneho znaku ak je nacitanych 8b zapisuje sa do suboru aktualizacia pocitadla
    * ahed->codedSize pre zaznam o kodovanych znakoch
    */
    for (int i = 0; i < code.size(); i++, counter--) {
        if (counter == -1) {
            counter = 7;
            codeBits.reset();
        }
        myBit = code[i] & 1;
        codeBits[counter] = myBit;

        if (counter == 0) {
            fputc(codeBits.to_ulong(), outputFile);
            ahed->codedSize++;
        }
    }
    //vyplnenie posledneho B az do konca nulami
    if (flag == 1 && counter != -1) {
        while (counter != 0) {
            codeBits[counter] = 0;
            counter--;
        }
        if (counter == 0) {
            fputc(codeBits.to_ulong(), outputFile);
            ahed->codedSize++;
        }
    }
}

NODE * isInTree(std::string c, NODE *root) {
    if( root->symbol.compare(c) == 0 )
        return root;
    else {
        NODE *current = nullptr;
        //uzol hladam najskor vlavo
        if (root->left != nullptr)
            current = isInTree(c, root->left);
        //ak sa nenasiel v lavych potomkoch ide sa hladat doprava
        if (current == nullptr)
        {
            if (root->right != nullptr)
                current = isInTree(c, root->right);
        }
        return current; //hladany prvok alebo nullptr
    }
}

std::string getCode(NODE *nodeToCode) {
    std::string code = "";
    NODE *par;
    NODE *act;
    act = nodeToCode;

    while(act->parent != nullptr) {
        par = act->parent;

        if (par->left == act) {
            code = "0" + code;
        } else if (par->right == act) {
            code = "1" + code;
        }
        act = par;
    }
    return code; //kod daneho uzla od korena k tomuto uzlu
}

void checkUpdate(NODE * root, NODE * tmpAct) {
    std::queue<NODE*> tmpQ;
    std::vector<NODE*> nodes;

    NODE * best;
    NODE * tmp;

    while(tmpAct != nullptr) {
        /*vyhladava v strome metodou BFS z prava dolava vsetky prvky ktore maju rovnaku vahu ako dany prvok
         * a nie su viac zanorene ako dany prvok prvky uklada do vectoru. Prvky suce ako kandidati na vymenu
         * musia splnat kriteria AHK musia mat rovnaku vahu ako dany prvok a kandidat nesmie byt priamy rodic
         * tohoto prvku.
         */
        for (tmpQ.push(root); !tmpQ.empty(); tmpQ.pop()) {
            NODE *tmp_node = tmpQ.front();
            if (tmp_node->weight == tmpAct->weight && tmp_node->left != tmpAct && tmp_node->right != tmpAct &&
                tmp_node != tmpAct->parent) {
                if (strlen(getCode(tmp_node).c_str()) > strlen(getCode(tmpAct).c_str()))
                    break;
                nodes.push_back(tmp_node);
            }
            if (tmp_node->right) {
                tmpQ.push(tmp_node->right);
            }

            if (tmp_node->left) {
                tmpQ.push(tmp_node->left);
            }
        }
        //vektor kandidatov nie je prazny, najvhodnejsi prvok je na zaciatku vektoru snim sa prehodia ukazatele
        if (!nodes.empty() && nodes.front() != tmpAct) {
            best = nodes.front();

            if (tmpAct->parent->left == tmpAct) {
                tmpAct->parent->left = best;
            } else if (tmpAct->parent->right == tmpAct) {
                tmpAct->parent->right = best;
            }

            if (best->parent->right == best) {
                best->parent->right = tmpAct;
            } else if (best->parent->left == best) {
                best->parent->left = tmpAct;
            }

            if (best->parent != tmpAct->parent) {
                tmp = best->parent;
                best->parent = tmpAct->parent;
                tmpAct->parent = tmp;
            }
        }
        nodes.clear();
        tmpAct->weight++;
        tmpAct = tmpAct->parent;
    }
}

void updateTree(NODE * root, std::string c) {
    NODE * tmpAct;
    NODE * given;
    given = isInTree(c, root);
    //dany prvok sa v strome nenachadza, vyhlada sa "NYT" z neho sa vytvoria dalsie 2 prvky a novy NYT a zadany prvok
    if (given == nullptr){
        NODE * parent;
        parent = isInTree("NYT", root);

        if (parent == nullptr) {
            std::cout << "update tree failed! NYT node is not located" << std::endl;
        } else {
            parent->symbol = "NODE";

            if (parent->left == nullptr) {
                NODE *lchild = new NODE;
                parent->left = lchild;
                lchild->left = nullptr;
                lchild->right = nullptr;
                lchild->parent = parent;
                lchild->symbol = "NYT";
                lchild->weight = 0;
            }

            if (parent->right == nullptr) {
                NODE *rchild = new NODE;
                parent->right = rchild;
                rchild->left = nullptr;
                rchild->right = nullptr;
                rchild->parent = parent;
                rchild->symbol = c;
                rchild->weight = 1;
            }
            tmpAct = parent;
            checkUpdate(root, tmpAct);
        }
    } else {//uz sa tam prvok nachadza, vola sa aktualizacia stromu
        tmpAct = given;
        checkUpdate(root, tmpAct);
    }
}

int AHEDEncoding(tAHED *ahed, FILE *inputFile, FILE *outputFile) {
    std::string code;
    std::string tmp_string;

    //inicializacia korena stromu
    NODE *root = new NODE;
    root ->left = nullptr;
    root ->right = nullptr;
    root ->parent = nullptr;
    root ->symbol = "NYT";
    root ->weight = 0;

    char c;
    //zo vstupneho suboru je nacitavane po znakoch, zvysuje sa pocitadlo nacianych nekodovanych znakov
    while((c = fgetc(inputFile)) != EOF) {
        ahed->uncodedSize++;
        tmp_string.push_back(c);
        //ak sa symbol v strome uz nachadza, ziska sa jeho kod a aktualizuje sa strom pre tento symbol
        //inak sa zyska kod prvku NYT a ascii hodnota noveho prvku, tento prvok sa vlozi do stromu na spravne miesto
        if(isInTree(tmp_string, root) != nullptr) {
            code = getCode(isInTree(tmp_string, root));
            updateTree(root, tmp_string);
        } else {
            code = getCode(isInTree("NYT", root));
            code += std::bitset<8>(c).to_string();
            updateTree(root, tmp_string);
        }

        writeCoded(ahed, code, outputFile, 0);
        code.clear();
        tmp_string.clear();
    }
    //na koniec suboru sa prida nova zarazka "End of text" hodnota "00000011"
    writeCoded(ahed, getCode(isInTree("NYT", root)), outputFile, 0);
    writeCoded(ahed, "00000011",outputFile, 1);
    deleteTree(root);
    return 0;
}

int AHEDDecoding(tAHED *ahed, FILE *inputFile, FILE *outputFile) {
    //inicializacia korena stromu
    NODE *root = new NODE;
    root ->left = nullptr;
    root ->right = nullptr;
    root ->parent = nullptr;
    root ->symbol = "NYT";
    root ->weight = 0;

    NODE *currentNode;
    currentNode = root;

    char c;
    bool cont = true;
    int pos = 7;
    int bit;
    std::string result;
    std::bitset<8> charInBit;
    std::bitset<8> resBit;

    c = fgetc(inputFile);
    charInBit = c;
    ahed->codedSize++;
    //znaky sa citaju zo suboru dokym sa nenarazi na zarazku (symulacia EOF) "00000011"
    while(cont) {
        //ak sa jedna o listovy uzol
        if (currentNode->left == nullptr && currentNode->right == nullptr) {
            //ak sa jedna o uzol s hodnotou "NYT"
            if( currentNode->symbol.compare("NYT") == 0 ) {
                resBit.reset();
                //zakodovany symbol este nie je v strome preto sa zo vstupu nacita 8b a ulozia sa do premennej
                //resBit, zvysuje sa pocitadlo nacitanych zakodovanych znakov
                for(int counter = 7; counter >= 0; counter--) {
                    resBit[counter] = charInBit[pos];
                    pos--;
                    if(pos == -1) {
                        pos = 7;
                        c = fgetc(inputFile);
                        charInBit = c;
                        ahed->codedSize++;
                    }
                }
                result = resBit.to_ulong();
                //ak bola nacitana zarazak konci sa nacitanie lebo bol dosiahnuty konec kodovanych znakov
                if (resBit == std::bitset<8>(3)){
                    cont = false;
                }
            } else { //dekoduje sa znak ktory sa uz v strome nachadza
                result = currentNode->symbol;
            }
            //ak este nebola dosiahnuta zarazka aktualizuje sa strom pre dekodovany znak a znak sa zapise do
            //vystupneho suboru a zvysuje sa pocitadlo dekodovanych znakov
            if (cont) {
                updateTree(root, result);
                currentNode = root;
                fputs(result.c_str(), outputFile);
                ahed->uncodedSize++;
            }
        } else { // nie je to listovy uzol, rozhodovanie na zaklade bitu zo vstupu 1 pokracovanie doprava 0 dolava
            bit = charInBit[pos] & 1;
            if (bit == 1) {
                currentNode = currentNode->right;
            } else if (bit == 0) {
                currentNode = currentNode->left;
            }
            pos--;
            //ak sa precital cely B nacita sa dalsi
            if(pos == -1) {
                pos = 7;
                c = fgetc(inputFile);
                charInBit = c;
                ahed->codedSize++;
            }
        }
    }
    deleteTree(root);
    return 0;
}
