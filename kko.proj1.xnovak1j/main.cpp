/*
 * Autor: Adrián Novák (xnovak1j)
 * Datum: 11.2.2018
 * Soubor: main.cpp
 * Komentar: Obsahuje spracovanie vstupnych parametrov programu a volanie prislusnych funkcii kodera a dekodera
 */

#include <iostream>
#include <getopt.h>
#include <fstream>
#include <cinttypes>
#include "ahed.h"

void print_help() {
    std::cout << "Uzivatelska napoveda!" << std::endl;
    std::cout <<
              "-i <ifile>:           Name of input file - if missing stdin is used\n"
              "-o <ofile>:           Name of output file - if missing stdout is used\n"
              "-l <logfile>:         Name of log file - if missing logging is ignored\n"
              "-c:                   Encoding option\n"
              "-x:                   Decoding option\n"
              "-h:                   Print help\n";
}

int main(int argc, char *argv[]) {
    /* vytvorenie prislusnych premennych, ktore su pouzite pri spracovani vstupnych parametrov programu
     * vstupne parametre su spracovane pomocou funkcie getopt_long. Na zaklade vstupnych parametrov su
     * nastavene obsluzne subory(vstup, vystup a log) a volane prislusne funkcie
     */
    bool is_inputFile = false;
    bool is_outputFile = false;
    bool is_logFile = false;
    bool is_c = false;
    bool is_x = false;
    bool is_h = false;

    std::string input_file;
    std::string output_file;
    std::string log_file;

    FILE *input;
    FILE *output;
    FILE *log;

    //tAHED struktura v ktorej su zasnamenane velkosti kodovanych a nekodovanych suborov
    tAHED *info = new tAHED;
    info->uncodedSize = 0;
    info->codedSize = 0;
    int result = 0;
    const char* const short_opts = "i:o:l:cxh";
    const option long_opts[] = {
            {"in", 1, nullptr, 'i'},
            {"out", 1, nullptr, 'o'},
            {"log", 1, nullptr, 'l'},
            {"encoding", 0, nullptr, 'c'},
            {"decoding", 0, nullptr, 'x'},
            {"help", 0, nullptr, 'h'},
            {nullptr, 0, nullptr, 0}
    };

    while (true)
    {
        const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

        if (-1 == opt)
            break;

        switch (opt)
        {
            case 'i':
                is_inputFile = true;
                input_file = std::string(optarg);
                break;
            case 'o':
                is_outputFile = true;
                output_file = std::string(optarg);
                break;
            case 'l':
                is_logFile = true;
                log_file = std::string(optarg);
                break;
            case 'c':
                is_c = true;
                break;
            case 'x':
                is_x = true;
                break;
            case 'h':
                is_h = true;
                print_help();
                break;
            case '?': // nerozpoznana volba
            default:
                print_help();
                break;
        }
    }
    //rozhodovanie na zaklade spracovanych vstupnych parametrov
    if(!is_h) {
        if (!is_inputFile) {
            input = stdin;
        } else {
            input = fopen(input_file.c_str(), "rb");
        }

        if (!is_outputFile) {
            output = stdout;
        } else {
            output = fopen(output_file.c_str(), "wb");
        }
        //volana funkcia kodovania vstupnych dat
        if (is_c) {
            result = AHEDEncoding(info, input, output);
        }
        //volana funkcia dekodovania vstupnych dat
        if (is_x) {
            result = AHEDDecoding(info, input, output);
        }
        if (is_logFile) {
            //zaznamenanie vysledkov do log file
            log = fopen(log_file.c_str(), "w");
            std::string logMessage;
            logMessage += "login = xnovak1j\nuncodedSize = ";
            logMessage += std::to_string(info->uncodedSize);
            logMessage += "\ncodedSize = ";
            logMessage += std::to_string(info->codedSize);
            logMessage += "\n";
            fputs(logMessage.c_str(),log);
            fclose(log);
        }

        delete(info);
        fclose(input);
        fclose(output);
    }
    return result;
}