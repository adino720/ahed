/*
 * Autor: Adrián Novák (xnovak1j)
 * Datum: 11.2.2018
 * Soubor: ahed.h
 * Komentar: definicie funkcii kniznice, dalej obsahuje zhrnutie funkcie prislusnych funkcii
 */

#ifndef KKO_PROJ1_XNOVAK1J_AHED_H
#define KKO_PROJ1_XNOVAK1J_AHED_H

#include <cstdint>
/* struktura NODE reprezentuje jeden uzol v huffmanovom strome v retazci symbol je uchovany znak ktory odpoveda
 *  prislusnymu uzlu v strome, weight urcuje vahu cize aktualny pocet vyskytov daneho znaku v nacitanom vstupe
 */
typedef struct node {
    struct node *parent;    //ukazatel na rodicovsky uzol
    struct node *left;      //ukazatel na laveho potomka
    struct node *right;     //ukazatel na praveho potomka
    std::string symbol;
    int weight;
}NODE;

/* Datovy typ zaznamu o (de)kodovani */
typedef struct{
    /* velikost nekodovaneho retezce */
    int64_t uncodedSize;
    /* velikost kodovaneho retezce */
    int64_t codedSize;
} tAHED;

//dealokacia stromu, vstupny parameter root je ukazatel na koren stromu, ktory sa ma rekurzivne dealokovat
void deleteTree(NODE *root);

/* zapis kodovanych dat do vystupneho suboru ahed obsahuje zaznam o velkosti kodovanych a nekodovanych datach
 * data sa do suboru zapisuju po celych B nie b preto je vyuzita premenna typu std::bitset<8> ktora po naplneni 8bitmi
 * je zapisana do suboru danym parametrom outputFile parameter flag ma hodnoty 0 a 1
 * 0 - jedna sa o klasicke zapisovane
 * 1 - jedna sa o posledne zapisovanie zapis vyrobeneho symbulu EOF - (End of text "00000011") a doplnenie B nulami
 */
void writeCoded(tAHED *ahed, std::string code, FILE *outputFile, int flag);

/* vyhladane prislusneho symbolu v Huffmanovom strome na zaklade symbolu(hodnota root->symbol)
 * ak sa dany symbol v strome nachadza, funkcia vrati prislusny uzol v ktorom je tato hodnota ina vrati nullptr
 */
NODE * isInTree(std::string c, NODE *root);

/*
 * funkcia prijima ukazatel na uzol stromu a jej ulohou je vratit  retazci jej kodovu hodnotu
 * funkcia prechadza od daneho prvku cez ukazatele na rodicovsky prvok az ku korenu a v pripade ze je uzol pravy
 * potomok tak hodnota pridana do kodu je 1 ak je lavy tak 0 kod symbolu sa slada odspodu stromu cize vysledny
 * retazec je skladany od zadu (z prava dolava)
 */
std::string getCode(NODE *nodeToCode);

/*
 * funkcia vykonava dynamicku aktializaciu huffmanovho stromu pre prislucny prvok, ktory dostava ako parameter tmpAct
 * fukncia vyhladava v strome od korena ktory dostane pomocou premennej root najvhodnejsieho kandidata pre vymenu
 * s aktualnym prvkom tmpAct strom prehladava pomocou metody BFS z prava dolava najlepsi prvok na vymenu pre prvok
 * s povodnou vahou rovnakou ako ma prvok tmpAct a je prvy najdeny metodou BFS, zaroven vsak nemoze byt tento prvok
 * rodicovsky prvok prvku tmpAct ak takyto prvok najde tak sa vzajomne prehodia ukazatele na tieto prvky a funkcia
 * sa zavola pre rodicovsky prvok pre aktualizaciu.
 */
void checkUpdate(NODE * root, NODE * tmpAct);

/*
 * uktializuje strom po nacitani prislusneho znaku. na vstupe ziska ukazatel na korenovy uzol a prislusny znak
 * pre ktory sa ma strom aktualizovat.
 * v pripade ze sa dany symbol v strome nenachadza tak sa vyhlada uzol s retazcom "NYT" not yet transmitted a tento
 * uzol sa rozdvoji, jeho lavy potomok bude novy "NYT" a pravy bude obsahovat dany znak, pre ktory sa strom aktualizuje
 * povodny "NYT" sa zmeni na "NODE"
 * ak sa znak v strome nachadza tak sa len aktualizuje jeho vaha a aktualizuje sa cely strom
 * POZN. tu je dovod pre odlisenie uzlu "NYT" a "NODE" od ostatnych kodovanych listovych uzlov preco je v strukture NODE
 * ->symbol std::string a nie char.
 */
void updateTree(NODE * root, std::string c);

/* Nazev:
 *   AHEDEncoding
 * Cinnost:
 *   Funkce koduje vstupni soubor do vystupniho souboru a porizuje zaznam o kodovani.
 * Parametry:
 *   ahed - zaznam o kodovani
 *   inputFile - vstupni soubor (nekodovany)
 *   outputFile - vystupní soubor (kodovany)
 * Navratova hodnota:
 *    0 - kodovani probehlo v poradku
 *    -1 - pøi kodovani nastala chyba
 */
int AHEDEncoding(tAHED *ahed, FILE *inputFile, FILE *outputFile);

/* Nazev:
 *   AHEDDecoding
 * Cinnost:
 *   Funkce dekoduje vstupni soubor do vystupniho souboru a porizuje zaznam o dekodovani.
 * Parametry:
 *   ahed - zaznam o dekodovani
 *   inputFile - vstupni soubor (kodovany)
 *   outputFile - vystupní soubor (nekodovany)
 * Navratova hodnota:
 *    0 - dekodovani probehlo v poradku
 *    -1 - pri dekodovani nastala chyba
 */
int AHEDDecoding(tAHED *ahed, FILE *inputFile, FILE *outputFile);

#endif //KKO_PROJ1_XNOVAK1J_AHED_H
